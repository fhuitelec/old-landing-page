.PHONY: help

help: ## Display this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

up: ## Start development environment
	docker-compose -f ./infrastructure/dev/docker-compose.yml -p about up -d

stop: ## Stop development environment
	docker-compose -f ./infrastructure/dev/docker-compose.yml -p about stop

php: ## Run PHP-FPM docker container using 'www-data' user
	docker exec -ti --user www-data about_php-fpm_1 sh

build:
	composer install --no-dev --optimize-autoloader --no-suggest
	npm install
	gulp

build-dev:
	composer install --optimize-autoloader --no-suggest
	npm install
	gulp