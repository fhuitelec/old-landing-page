<?php

namespace About\AboutBundle\Controller;

use About\AboutBundle\Helper\LinkHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="about")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $todoList = [
            [
                'checked' => '',
                'item' => 'Add Docker, PHP7 and HTTP/2 kind of badges'
            ],
            [
                'checked' => 'checked',
                'item' => 'Upgrade to PHP7'
            ],
            [
                'checked' => '',
                'item' => 'Enable HTTP/2 and server push'
            ],
            [
                'checked' => 'checked',
                'item' => 'Move private Gitlab repo to a Github public one'
            ],
            [
                'checked' => '',
                'item' => 'Create a resume section'
            ],
            [
                'checked' => '',
                'item' => 'Create a form for the contact section'
            ],
            [
                'checked' => 'checked',
                'item' => 'Change the ugly Bootstrap flash alert'
            ],
            [
                'checked' => 'checked',
                'item' => 'Create Symfony bootstrap'
            ],
            [
                'checked' => 'checked',
                'item' => 'Apply theme'
            ],
            [
                'checked' => 'checked',
                'item' => 'Create flash alert on non-built components'
            ],
            [
                'checked' => 'checked',
                'item' => 'Find a neat background picture and photoshop it'
            ],
        ];

        return $this->render('default/index.html.twig', [
            'todoList' => $todoList,
        ]);
    }

    /**
     * @Route("/blog", name="blog")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blogAction(Request $request)
    {
        return $this->redirect(LinkHelper::$BLOG_URL);
    }

    /**
     * @Route("/contact", name="contact")
     * @Route("/contact/", name="contact_root")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactAction(Request $request)
    {
        return $this->redirect(LinkHelper::$GET_IN_TOUCH_URL);
    }
}
