<?php
namespace About\AboutBundle\Helper;

class LinkHelper
{
    public static $BLOG_URL = 'http://blog.huitelec.fr/';
    public static $GET_IN_TOUCH_URL = 'mailto:fabien@huitelec.fr?subject=About fabien - get in touch';
}