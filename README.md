# About me
Source code of my landing page [about.huitelec.fr](http://about.huitelec.fr/)

## :warning: Not maintained anymore

A new version of this landing page powered by Jekyll and Gitlab pages is available [here](https://gitlab.com/fhuitelec/landing-page) ([website](https://about.huitelec.fr)).

## Getting started
This is still in heavy development, but if you want to play around, the following commands should do the job

```
composer install
npm install
gulp
make dev-up
```

You can hit it on `about.dev`. Make sure you have binded your docker host IP to it in `/etc/hosts`.

And that's all!

## Tests
No tests yet!

## License
Copyright 2016 © Fabien Huitelec. Code released under the [MIT](https://github.com/fhuitelec/about-me/blob/gh-pages/LICENSE) license.

## Contributions
I accept issues/PR on typos and issues on suggestions.

## Thanks
[BlackrockDigital](https://github.com/BlackrockDigital/) for their amazing and well-coded [**clean blog**](https://startbootstrap.com/template-overviews/clean-blog/) theme on [startbootstrap.com](http://startbootstrap.com/) [[source](https://github.com/BlackrockDigital/startbootstrap-clean-blog/)]
