FROM php:7.0.14-fpm-alpine

MAINTAINER Fabien Huitelec < fabien at huitelec dot fr >

ARG SYMFONY_SECRET

ENV SYMFONY_ENV=prod
ENV NODE_ENV=production

# Add dependencies, assets and files non cache sensitive
RUN mkdir /app
ADD package.json yarn.lock composer.json composer.lock gulpfile.js /app/
ADD app/ /app/app/
ADD web/ /app/web/
RUN chown -R www-data:www-data /app
WORKDIR /app

# Install & build dependencies
RUN apk add --update nodejs-lts \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
USER www-data
RUN curl -sS https://yarnpkg.com/install.sh|sh \
    && export PATH="$HOME/.yarn/bin:$PATH" \
    && composer global require "hirak/prestissimo:^0.3" \
    && composer install --no-dev --optimize-autoloader --no-suggest --no-progress --profile --prefer-dist \
    && yarn install --no-progress \
    && node_modules/gulp/bin/gulp.js \
    && rm -rf node_modules

# Add the rest of the files
USER root
ADD src/ /app/src/
ADD var/ /app/var/
RUN chown -R www-data:www-data /app/src \
    && chown -R www-data:www-data /app/var

USER www-data